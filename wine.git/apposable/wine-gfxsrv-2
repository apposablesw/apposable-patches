diff --git a/dlls/winesdl.drv/gfxclient.c b/dlls/winesdl.drv/gfxclient.c
new file mode 100644
index 0000000..6c3c9ba
--- /dev/null
+++ b/dlls/winesdl.drv/gfxclient.c
@@ -0,0 +1,240 @@
+/*
+ * SDL Graphics Server for Wine - Client Interface
+ *
+ * Copyright (C) 2012 David Olofson, for Apposable Software
+ *
+ *	Overview:
+ *		This is the implementation of the client interface to the
+ *		Graphics Server. It maps the shared memory area provided by the
+ *		server into the local process address space and handles the
+ *		low level details of the client/server interaction.
+ */
+
+#include "config.h"
+
+#include <stdarg.h>
+#include <string.h>
+#include <stdio.h>
+#include <errno.h>
+#include <sys/mman.h>
+#include <fcntl.h>
+
+#include "sdldrv.h"
+
+#include "windef.h"
+#include "winbase.h"
+#include "wingdi.h"
+#include "wine/debug.h"
+#include "gdi32/gdi_private.h"
+
+WINE_DEFAULT_DEBUG_CHANNEL(gfxclient);
+
+GFXSRV_serverinfo *serverinfo = NULL;
+
+/*
+ * Release framebuffer and desktop info mappings
+ */
+static void release_screen(BOOL force)
+{
+	unsigned r = __sync_add_and_fetch(&serverinfo->clients, -1);
+	TRACE("Detaching from graphics server. %d clients remaining.\n", r);
+	if(force)
+	{
+		TRACE("Forcing shutdown!\n");
+		serverinfo->running = 0;
+	}
+	munmap(serverinfo, serverinfo->regioninfo.region_size);
+	serverinfo = NULL;
+}
+
+/*
+ * Try to find existing graphics server and framebuffer
+ */
+static BOOL get_screen(void)
+{
+	unsigned clients;
+	int handle;
+	GFXSRV_regioninfo ri;
+	FILE *f = fopen(GFXSRV_REGIONINFO_PATH, "rb");
+	if(!f)
+	{
+		ERR("Could not open \"%s\"!\n", GFXSRV_REGIONINFO_PATH);
+		return FALSE;
+	}
+	if(fread(&ri, sizeof(ri), 1, f) != 1)
+	{
+		ERR("Could not read \"%s\"!\n", GFXSRV_REGIONINFO_PATH);
+		fclose(f);
+		return FALSE;
+	};
+	fclose(f);
+	TRACE("Found a graphics server!\n");
+	TRACE("   Version %d.%d.%d build %d\n",
+			GFXSRV_MAJOR(ri.server_version),
+			GFXSRV_MINOR(ri.server_version),
+			GFXSRV_MICRO(ri.server_version),
+			GFXSRV_BUILD(ri.server_version));
+	TRACE("    Region size: %d\n", ri.region_size);
+
+	if((GFXSRV_MAJOR(ri.server_version) !=
+			GFXSRV_MAJOR(GFXSRV_SERVER_VERSION)) ||
+			(GFXSRV_MINOR(ri.server_version) !=
+			GFXSRV_MINOR(GFXSRV_SERVER_VERSION)))
+	{
+		ERR("Incompatible graphics server version!\n");
+		return FALSE;
+	}
+
+	handle = open(GFXSRV_REGION_PATH, O_RDWR);
+	if(handle < 0)
+	{
+		ERR("Could not open the graphics server shared memory "
+				"region! (%s)\n", strerror(errno));
+		return FALSE;
+	}
+	serverinfo = mmap(NULL, ri.region_size, PROT_READ | PROT_WRITE,
+			MAP_SHARED, handle, 0);
+	close(handle);
+	if(serverinfo == MAP_FAILED)
+	{
+		ERR("Could not map the graphics server shared memory "
+				"region! (%s)\n", strerror(errno));
+		return FALSE;
+	}
+	TRACE("GFXSRV_serverinfo at %p\n", serverinfo);
+
+	clients = __sync_add_and_fetch(&serverinfo->clients, 1);
+	TRACE("Attached to graphics server. %d clients.\n", clients);
+	return TRUE;
+}
+
+BOOL SDLDRV_GetScreen(void)
+{
+	BOOL res;
+	if(serverinfo)
+		return TRUE;	/* Already open! */
+	SDLDRV_Lock();
+	res = get_screen();
+	SDLDRV_Unlock();
+	return res;
+}
+
+void SDLDRV_MoveView(HBITMAP view, const RECT *rect)
+{
+	BITMAPOBJ *bmp = GDI_GetObjPtr(view, OBJ_BITMAP);
+	int x, y, w, h;
+	if(!bmp)
+		return;
+	if(rect)
+	{
+		x = rect->left;
+		y = rect->top;
+		w = serverinfo->width;
+		if(rect->bottom <= rect->top)
+			h = 0;
+		else
+			h = -(rect->bottom - rect->top);
+	}
+	else
+	{
+		x = y = 0;
+		w = serverinfo->width;
+		h = -serverinfo->height;
+	}
+	bmp->size.cx = bmp->dib.dsBmih.biWidth = bmp->dib.dsBm.bmWidth = w;
+	bmp->size.cy = bmp->dib.dsBmih.biHeight = bmp->dib.dsBm.bmHeight = h;
+	bmp->dib.dsBm.bmWidthBytes = w * (serverinfo->bpp / 8);
+	bmp->dib.dsBm.bmBits = (void *)(
+			(char *)gfxsrv_GetFrameBuffer(serverinfo) +
+			y * bmp->dib.dsBm.bmWidthBytes +
+			x * (serverinfo->bpp / 8)
+			);
+	TRACE("!!! %d,%d %dx%d %p\n", x, y, w, h, bmp->dib.dsBm.bmBits);
+	GDI_ReleaseObj(view);
+	/*
+	 * NOTE:
+	 *	After this, we also need to make sure that init_dib_info() in
+	 *	dc.c of the DIB engine is called somehow. Any cached dib_info
+	 *	structs will break things!
+	 */
+}
+
+HBITMAP SDLDRV_CreateView(HDC hdc, const RECT *rect)
+{
+	BITMAPINFOHEADER bih;
+	HBITMAP hbm;
+	BITMAPOBJ *bmp;
+	TRACE("%s\n", wine_dbgstr_rect(rect));
+	bih.biSize = sizeof(bih);
+	bih.biWidth = 1;
+	bih.biHeight = 1;
+	bih.biPlanes = 1;
+	bih.biBitCount = 32;
+	bih.biCompression = BI_RGB;
+	bih.biSizeImage = 0;
+	bih.biXPelsPerMeter = 0;
+	bih.biYPelsPerMeter = 0;
+	bih.biClrUsed = 0;
+	bih.biClrImportant = 0;
+	hbm = CreateDIBSection(hdc, (BITMAPINFO *)&bih, DIB_RGB_COLORS,
+			NULL, NULL, 0);
+	if(!(bmp = GDI_GetObjPtr(hbm, OBJ_BITMAP)))
+	{
+		DeleteObject(hbm);
+		return 0;
+	}
+	HeapFree(GetProcessHeap(), 0, bmp->dib.dsBm.bmBits);
+	bmp->dib.dsBm.bmBits = NULL;
+	GDI_ReleaseObj(bmp);
+	SDLDRV_MoveView(hbm, rect);
+	return hbm;
+}
+
+void SDLDRV_DeleteView(HBITMAP view)
+{
+	BITMAPOBJ *bmp = GDI_GetObjPtr(view, OBJ_BITMAP);
+	if(!bmp)
+		return;
+	bmp->dib.dsBm.bmBits = NULL;
+	GDI_ReleaseObj(view);
+	DeleteObject(view);
+}
+
+BOOL SDLDRV_InitGDI(void)
+{
+	return SDLDRV_GetScreen();
+}
+
+
+/**********************************************************************
+ *	SDLDRV_GDI_Finalize
+ */
+void SDLDRV_GDI_Finalize(void)
+{
+	SDLDRV_Lock();
+	release_screen(0);
+	SDLDRV_Unlock();
+}
+
+
+/*
+ * All we can do here now is try to find the graphics server and return TRUE if
+ * that succeeds. Could request a desktop resize or something from here later.
+ *
+ * Oh, and we need to tell MsgWaitForMultipleObjectsEx() that this is the
+ * thread that's supposed to process the input events from the graphics server!
+ */
+unsigned long CDECL SDLDRV_CreateDesktop(UINT width, UINT height)
+{
+	SDLDRV_thread_data *data = sdldrv_thread_data();
+	data->is_desktop = TRUE;
+	return SDLDRV_GetScreen();
+}
+
+
+void SDLDRV_ShutdownDesktop(void)
+{
+	SDLDRV_Lock();
+	release_screen(1);
+	SDLDRV_Unlock();
+}
diff --git a/dlls/winesdl.drv/gfxsrvutil.c b/dlls/winesdl.drv/gfxsrvutil.c
new file mode 100644
index 0000000..5ce4761
--- /dev/null
+++ b/dlls/winesdl.drv/gfxsrvutil.c
@@ -0,0 +1,50 @@
+/*
+ * SDL Graphics Server for Wine - Client/Server Interface
+ *
+ * Copyright (C) 2012 David Olofson, for Apposable Software
+ */
+
+#include "gfxsrv.h"
+
+
+#define	ROWMASK	((1 << (GFXSRV_HTILES)) - 1)
+
+void gfxsrv_MarkDirty(GFXSRV_serverinfo *si, int left, int top, int right,
+		int bottom)
+{
+#ifdef GFXSRV_SIMPLEREFRESH
+	si->dirty = 1;
+#else
+	unsigned newbits, row;
+
+	/* Calculate bit mask for one row of tiles */
+	left /= si->tilew;
+#  if 0
+	if(left < 0)
+		left = 0;
+#  endif
+	right /= si->tilew;
+	if(right > GFXSRV_HTILES - 1)
+		right = GFXSRV_HTILES - 1;
+	row = (ROWMASK << left) & (ROWMASK >> (GFXSRV_HTILES - right - 1));
+
+	/* Apply bit mask to the affected rows */
+	top /= si->tileh;
+#  if 0
+	if(top < 0)
+		top = 0;
+#  endif
+	bottom /= si->tileh;
+#  if 0
+	if(bottom > SDLDRV_VTILES - 1)
+		bottom = SDLDRV_VTILES - 1;
+#  endif
+
+	row <<= GFXSRV_HTILES * top;
+	for(newbits = 0; top <= bottom; ++top, row <<= GFXSRV_HTILES)
+		newbits |= row;
+
+	/* Pass dirty bits to the refresh server */
+	__sync_or_and_fetch(&si->dirty, newbits);
+#endif
+}
diff --git a/dlls/winesdl.drv/sfifo.c b/dlls/winesdl.drv/sfifo.c
new file mode 100644
index 0000000..a9b7117
--- /dev/null
+++ b/dlls/winesdl.drv/sfifo.c
@@ -0,0 +1,280 @@
+/*
+------------------------------------------------------------
+   SFIFO 2.0 - Simple portable lock-free FIFO
+------------------------------------------------------------
+ * Copyright (C) 2000-2009, 2012 David Olofson
+ *
+ * This software is provided 'as-is', without any express or
+ * implied warranty. In no event will the authors be held
+ * liable for any damages arising from the use of this
+ * software.
+ *
+ * Permission is granted to anyone to use this software for
+ * any purpose, including commercial applications, and to
+ * alter it and redistribute it freely, subject to the
+ * following restrictions:
+ *
+ * 1. The origin of this software must not be misrepresented;
+ *    you must not claim that you wrote the original
+ *    software. If you use this software in a product, an
+ *    acknowledgment in the product documentation would be
+ *    appreciated but is not required.
+ * 2. Altered source versions must be plainly marked as such,
+ *    and must not be misrepresented as being the original
+ *    software.
+ * 3. This notice may not be removed or altered from any
+ *    source distribution.
+ */
+
+#include "sfifo.h"
+
+#include <string.h>
+#include <stdlib.h>
+#			include <stdio.h>
+
+#ifdef _SFIFO_TEST_
+#include <stdio.h>
+#include <unistd.h>
+#include <pthread.h>
+#define DBG(x)	/*(x)*/
+#define TEST_BUFSIZE	10
+#else
+#define DBG(x)
+#endif
+
+
+SFIFO *sfifo_Open(unsigned size)
+{
+	SFIFO *f;
+	unsigned bsize;
+	if(size > SFIFO_MAX_BUFFER_SIZE)
+		return NULL;	/* Too large buffer! */
+
+	/*
+	 * Set sufficient power-of-2 size.
+	 *
+	 * No, there's no bug. If you need
+	 * room for N bytes, the buffer must
+	 * be at least N+1 bytes. (The fifo
+	 * can't tell 'empty' from 'full'
+	 * without unsafe index manipulations
+	 * otherwise.)
+	 */
+	for(bsize = 1; bsize <= size; bsize <<= 1)
+		;
+
+	f = (SFIFO *)malloc(sizeof(SFIFO) + bsize);
+	if(!f)
+		return NULL;
+	f->size = bsize;
+	f->readpos = f->writepos = 0;
+	f->flags = SFIFO_IS_OPEN | SFIFO_FREE_MEMORY;
+	return f;
+}
+
+
+SFIFO *sfifo_Init(void *mem, unsigned memsize)
+{
+	SFIFO *f = (SFIFO *)mem;
+	unsigned bsize;
+	memsize -= sizeof(SFIFO);
+	if(memsize > SFIFO_MAX_BUFFER_SIZE)
+		memsize = SFIFO_MAX_BUFFER_SIZE;
+	for(bsize = 1; bsize <= memsize; bsize <<= 1)
+		;
+	bsize >>= 1;
+	f->size = bsize;
+	f->readpos = f->writepos = 0;
+	f->flags = SFIFO_IS_OPEN;
+	return f;
+}
+
+
+void sfifo_Close(SFIFO *f)
+{
+	if(!(f->flags & SFIFO_IS_OPEN))
+		return;	/* Already closed! */
+	if(f->flags & SFIFO_FREE_MEMORY)
+	{
+		memset(f, 0, sizeof(SFIFO));
+		free(f);
+	}
+	else
+		memset(f, 0, sizeof(SFIFO));
+}
+
+
+void sfifo_Flush(SFIFO *f)
+{
+	f->readpos = 0;
+	f->writepos = 0;
+}
+
+
+int sfifo_Write(SFIFO *f, const void *_buf, unsigned len)
+{
+	unsigned total;
+	int i;
+	const char *buf = (const char *)_buf;
+	char *buffer = (char *)(f + 1);
+
+	if(!(f->flags & SFIFO_IS_OPEN))
+		return SFIFO_CLOSED;
+
+	/* total = len = min(space, len) */
+	total = sfifo_Space(f);
+	if(len > total)
+		len = total;
+	else
+		total = len;
+
+	i = f->writepos;
+	if(i + len > f->size)
+	{
+		memcpy(buffer + i, buf, f->size - i);
+		buf += f->size - i;
+		len -= f->size - i;
+		i = 0;
+	}
+	memcpy(buffer + i, buf, len);
+	f->writepos = i + len;
+
+	return (int)total;
+}
+
+
+int sfifo_Read(SFIFO *f, void *_buf, unsigned len)
+{
+	unsigned total;
+	int i;
+	char *buf = (char *)_buf;
+	char *buffer = (char *)(f + 1);
+
+	if(!(f->flags & SFIFO_IS_OPEN))
+		return SFIFO_CLOSED;
+
+	/* total = len = min(used, len) */
+	total = sfifo_Used(f);
+	if(len > total)
+		len = total;
+	else
+		total = len;
+
+	i = f->readpos;
+	if(i + len > f->size)
+	{
+		memcpy(buf, buffer + i, f->size - i);
+		buf += f->size - i;
+		len -= f->size - i;
+		i = 0;
+	}
+	memcpy(buf, buffer + i, len);
+	f->readpos = i + len;
+
+	return (int)total;
+}
+
+
+int sfifo_WriteSpin(SFIFO *f, const void *buf, unsigned len)
+{
+	while(sfifo_Space(f) < len)
+		;
+	return sfifo_Write(f, buf, len);
+}
+
+
+int sfifo_ReadSpin(SFIFO *f, void *buf, unsigned len)
+{
+	while(sfifo_Used(f) < len)
+		;
+	return sfifo_Read(f, buf, len);
+}
+
+
+#ifdef _SFIFO_TEST_
+void *sender(void *arg)
+{
+	char buf[TEST_BUFSIZE*2];
+	int i,j;
+	int cnt = 0;
+	int res;
+	SFIFO *sf = (SFIFO *)arg;
+	while(1)
+	{
+		j = sfifo_Space(sf);
+		for(i = 0; i < j; ++i)
+		{
+			++cnt;
+			buf[i] = cnt;
+		}
+		res = sfifo_Write(sf, &buf, j);
+		if(res != j)
+		{
+			printf("Write failed!\n");
+			sleep(1);
+		} else if(res)
+			printf("Wrote %d\n", res);
+	}
+}
+int main()
+{
+	SFIFO *sf;
+	char last = 0;
+	pthread_t thread;
+	char buf[100] = "---------------------------------------";
+#if 0
+	char mem[1000];
+	sf = sfifo_Init(&mem, sizeof(mem));
+	printf("sfifo_Init(%p, %lu) = %p\n", &mem, sizeof(mem), sf);
+#else
+	sf = sfifo_Open(TEST_BUFSIZE);
+	printf("sfifo_Open(%d) = %p\n", TEST_BUFSIZE, sf);
+#endif
+
+#if 0
+	printf("sfifo_Write(sf, \"0123456789\", 7) = %d\n",
+		sfifo_Write(sf, "0123456789", 7) );
+
+	printf("sfifo_Write(sf, \"abcdefghij\", 7) = %d\n",
+		sfifo_Write(sf, "abcdefghij", 7) );
+
+	printf("sfifo_Read(sf, buf, 8) = %d\n",
+		sfifo_Read(&sf, buf, 8) );
+
+	buf[20] = 0;
+	printf("buf =\"%s\"\n", buf);
+
+	printf("sfifo_Write(sf, \"0123456789\", 7) = %d\n",
+		sfifo_Write(sf, "0123456789", 7) );
+
+	printf("sfifo_Read(sf, buf, 10) = %d\n",
+		sfifo_Read(sf, buf, 10) );
+
+	buf[20] = 0;
+	printf("buf =\"%s\"\n", buf);
+#else
+	pthread_create(&thread, NULL, sender, sf);
+
+	while(1)
+	{
+		static int c = 0;
+		++last;
+		while(sfifo_Read(sf, buf, 1) != 1)
+			/*sleep(1)*/;
+		if(last != buf[0])
+		{
+			printf("Error %d!\n", buf[0] - last);
+			last = buf[0];
+		}
+		else
+			printf("Ok. (%d)\n", ++c);
+	}
+#endif
+
+	sfifo_Close(sf);
+	printf("sfifo_Close(sf)\n");
+
+	return 0;
+}
+
+#endif
diff --git a/dlls/winesdl.drv/sfifo.h b/dlls/winesdl.drv/sfifo.h
new file mode 100644
index 0000000..e5f9226
--- /dev/null
+++ b/dlls/winesdl.drv/sfifo.h
@@ -0,0 +1,127 @@
+/*
+------------------------------------------------------------
+   SFIFO 2.0 - Simple portable lock-free FIFO
+------------------------------------------------------------
+ * Copyright (C) 2000-2009, 2012 David Olofson
+ *
+ * This software is provided 'as-is', without any express or
+ * implied warranty. In no event will the authors be held
+ * liable for any damages arising from the use of this
+ * software.
+ *
+ * Permission is granted to anyone to use this software for
+ * any purpose, including commercial applications, and to
+ * alter it and redistribute it freely, subject to the
+ * following restrictions:
+ *
+ * 1. The origin of this software must not be misrepresented;
+ *    you must not claim that you wrote the original
+ *    software. If you use this software in a product, an
+ *    acknowledgment in the product documentation would be
+ *    appreciated but is not required.
+ * 2. Altered source versions must be plainly marked as such,
+ *    and must not be misrepresented as being the original
+ *    software.
+ * 3. This notice may not be removed or altered from any
+ *    source distribution.
+ */
+
+#ifndef SFIFO_H
+#define	SFIFO_H
+
+#ifdef __cplusplus
+extern "C" {
+#endif
+
+/*------------------------------------------------
+	"Private" stuff
+------------------------------------------------*/
+/*
+ * Porting note:
+ *	Reads and writes of a variable of this type in memory
+ *	must be *atomic*! 'int' is *not* atomic on all platforms.
+ *	A safe type should be used, and  sfifo should limit the
+ *	maximum buffer size accordingly.
+ */
+typedef volatile int SFIFO_ATOMIC;
+
+/* Kludge: Assume 32 bit platform */
+#define	SFIFO_MAX_BUFFER_SIZE	0x7fffffff
+
+/* (flags) Set if SFIFO is initialized and open */
+#define	SFIFO_IS_OPEN		0x00000001
+
+/* (flags) Set if SFIFO was allocated using malloc() */
+#define	SFIFO_FREE_MEMORY	0x00000002
+
+typedef struct SFIFO
+{
+	unsigned	size;		/* Number of bytes */
+	unsigned	flags;
+	SFIFO_ATOMIC	readpos;	/* Read position */
+	SFIFO_ATOMIC	writepos;	/* Write position */
+	/* (Buffer follows this structure!) */
+} SFIFO;
+
+#define SFIFO_SIZEMASK(x)	((x)->size - 1)
+
+
+/*------------------------------------------------
+	API
+------------------------------------------------*/
+
+typedef enum
+{
+	SFIFO_MEMORY =	-1,
+	SFIFO_CLOSED =	-2
+} SFIFO_errors;
+
+/* Allocate and initialize FIFO that fits at least 'size' bytes of data. */
+SFIFO *sfifo_Open(unsigned size);
+
+/* Create the largest possible FIFO in the specified memory area. */
+SFIFO *sfifo_Init(void *mem, unsigned memsize);
+
+/* Close and (where applicable) deallocate the specified FIFO. */
+void sfifo_Close(SFIFO *f);
+
+/* Clear the FIFO. NOTE: NOT thread safe! */
+void sfifo_Flush(SFIFO *f);
+
+/*
+ * Write up to 'len' bytes to FIFO 'f'. Returns the number of bytes written,
+ * or a negative error code.
+ */
+int sfifo_Write(SFIFO *f, const void *buf, unsigned len);
+
+/*
+ * Read up to 'len' bytes from FIFO 'f'. Returns the number of bytes actually
+ * read, or a negative error code.
+ */
+int sfifo_Read(SFIFO *f, void *buf, unsigned len);
+
+/*
+ * Spinning versions of sfifo_Read() and sfifo_Write().
+ *
+ * NOTE:
+ *	These are BUSY-WAITING versions that should ONLY be used on
+ *	multiprocessor systems, in applications where one CPU will
+ *	occasionally wait for data from another for brief moments!
+ */
+int sfifo_WriteSpin(SFIFO *f, const void *buf, unsigned len);
+int sfifo_ReadSpin(SFIFO *f, void *buf, unsigned len);
+
+static inline int sfifo_Used(SFIFO *f)
+{
+	return (f->writepos - f->readpos) & SFIFO_SIZEMASK(f);
+}
+static inline int sfifo_Space(SFIFO *f)
+{
+	return f->size - 1 - sfifo_Used(f);
+}
+
+#ifdef __cplusplus
+};
+#endif
+
+#endif /* SFIFO_H */
